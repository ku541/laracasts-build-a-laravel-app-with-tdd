<?php

/**
 * Generate a URL to a gravatar thumbnail.
 *
 * @param  string $email
 * @return string
 */
function gravatar_url($email)
{
    return "https://gravatar.com/avatar/" . md5($email) . "?s=60";
}
