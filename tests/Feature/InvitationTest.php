<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Facades\Tests\Setup\ProjectFactory;
use Tests\TestCase;

class InvitationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function non_owners_may_not_invit_users()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $user = factory('App\User')->create();

        $assertInvitationForbidden = function () use ($user, $project) {
            $this->actingAs($user)
                ->post($project->path() . '/invitations')
                ->assertStatus(403);
        };

        $assertInvitationForbidden();

        $project->invite($user);

        $assertInvitationForbidden();
    }

    /** @test */
    function a_project_owner_can_invite_a_user()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $userToInvite = factory('App\User')->create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/invitations', [
            'email' => $userToInvite->email
            ])
            ->assertRedirect($project->path());

        $this->assertTrue($project->members->contains($userToInvite));
    }

    /** @test */
    function the_email_address_must_be_associated_with_a_valid_birdboard_account()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/invitations', [
                'email' => 'notauser@example.com'
            ])
            ->assertSessionHasErrors([
                'email' => 'The user you are inviting must have a birdboard account.'
            ], null, 'invitations');
    }

    /** @test */
    function invited_users_may_update_project_details()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $project->invite($newUser = factory('App\User')->create());

        $this->signIn($newUser);

        $this->post(
            action('ProjectTasksController@store', $project),
            $task = ['body' => 'Test Task']
        );

        $this->assertDatabaseHas('tasks', $task);
    }
}
