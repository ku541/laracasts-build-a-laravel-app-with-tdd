<?php

namespace Tests\Feature;

use App\Project;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectTaskTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function guests_cannot_add_tasks_to_projects()
    {
        // $this->withoutExceptionHandling();

        $project = factory(Project::class)->create();

        $this->post($project->path() . '/tasks')->assertRedirect('login');
    }

    /** @test */
    function only_the_owner_of_a_project_may_add_tasks()
    {
        // $this->withoutExceptionHandling();

        $this->signIn();

        $project = factory(Project::class)->create();

        $attributes = ['body' => 'Test task'];

        $this->post($project->path() . '/tasks', $attributes)
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', $attributes);
    }

    /** @test */
    function only_the_owner_of_a_project_may_update_a_tasks()
    {
        // $this->withoutExceptionHandling();

        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $attributes = ['body' => 'Changed'];

        $this->patch($project->tasks->first()->path(), $attributes)
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', $attributes);
    }

    /** @test */
    function a_project_can_have_tasks()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', ['body' => 'Test task']);

        $this->get($project->path())->assertSee('Test task');
    }

    /** @test */
    function a_task_can_be_updated()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::withTasks(1)->create();

        $attributes = [
            'body' => 'Changed'
        ];

        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), $attributes);

        $this->assertDatabaseHas('tasks', $attributes);
    }

    /** @test */
    function a_task_can_be_completed()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::withTasks(1)->create();

        $attributes = [
            'body' => 'Changed',
            'completed' => true
        ];

        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), $attributes);

        $this->assertDatabaseHas('tasks', $attributes);
    }

    /** @test */
    function a_task_can_be_marked_as_incomplete()
    {
        $this->withoutExceptionHandling();

        $project = ProjectFactory::withTasks(1)->create();

        $attributes = [
            'body' => 'Changed',
            'completed' => true
        ];

        $this->actingAs($project->owner)
            ->patch($project->tasks->first()->path(), $attributes);

        $attributes['completed'] = false;

        $this->patch($project->tasks->first()->path(), $attributes);

        $this->assertDatabaseHas('tasks', $attributes);
    }

    /** @test */
    function a_task_requires_a_body()
    {
        // $this->withoutExceptionHandling();

        $project = ProjectFactory::create();

        $attributes = factory('App\Task')->raw(['body' => '']);

        $this->actingAs($project->owner)
            ->post($project->path() . '/tasks', $attributes)
            ->assertSessionHasErrors('body');
    }
}
