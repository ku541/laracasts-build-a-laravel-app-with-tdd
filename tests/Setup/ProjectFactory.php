<?php

namespace Tests\Setup;

use App\User;
use App\Project;

class ProjectFactory
{
    /**
     * The number of tasks that should be created for the project.
     *
     * @var int
     */
    protected $tasksCount = 0;

    /**
     * The owner of the project.
     *
     * @var User
     */
    protected $user;

    /**
     * Set the number of tasks that should be created for the project.
     *
     * @param  int  $count
     * @return $this
     */
    public function withTasks($count)
    {
        $this->tasksCount = $count;

        return $this;
    }

    /**
     * Set the owner of the project.
     *
     * @param  User  $user
     * @return $this
     */
    public function ownedBy(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Create the world for testing.
     *
     * @return Project $project
     */
    public function create()
    {
        $project = factory(Project::class)->create([
            'owner_id' => $this->user ?? factory('App\User')
        ]);

        factory('App\Task', $this->tasksCount)->create([
            'project_id' => $project->id
        ]);

        return $project;
    }
}
