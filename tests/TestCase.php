<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Authenticate a user.
     *
     * @param  User $user
     * @return User $user
     */
    public function signIn(User $user = null)
    {
        $user = $user ?? factory('App\User')->create();

        $this->actingAs($user);

        return $user;
    }
}
